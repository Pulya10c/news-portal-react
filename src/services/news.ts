import { INews } from '../common/models/news';

interface IRequestServiceArg {
  url: string;
  method: string;
  query?: string;
  news?: INews;
}

class RequestService {
  getResponse({ url, method, query, news }: IRequestServiceArg) {
    let search = '';
    let body: string;
    let requestOptions: { [key: string]: string };
    switch (method) {
      case 'PUT':
      case 'POST':
        body = JSON.stringify(news);
        requestOptions = { method, body, 'Content-Type': 'application/json' };
        break;
      case 'DELETE':
        requestOptions = { method, 'Content-Type': 'application/json' };
        break;
      default:
        search = query ? `?query=${query}` : '';
        requestOptions = { method, 'Content-Type': 'application/json' };
        break;
    }
    return this._asyncLoad({ requestOptions, url: url + search });
  }

  async _asyncLoad({ requestOptions, url }: { url: string; requestOptions: { [key: string]: string } }) {
    try {
      const baseURL = `https://t9m1ddcsri.execute-api.eu-west-1.amazonaws.com/dev${url}`;
      const res = await fetch(baseURL, requestOptions);
      return await res.json();
    } catch (err) {
      console.error(err);
    }
  }
}

export default new RequestService();
