export interface IEditNews {
  source?: {
    id: string;
    name: string;
  };
  id?: string;
  author: string;
  title: string;
  description: string;
  content: string;
  url?: undefined;
  urlToImage?: undefined;
  publishedAt?: number | string;
}
