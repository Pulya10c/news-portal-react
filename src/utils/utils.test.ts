import { sortByDate } from './utils';
import { mockNews } from '../common/constants';

const mockNewsSort = [
  {
    source: {
      id: '',
      name: '',
    },
    url: '',
    urlToImage: '',
    author: 'Aleh Serhiyenia',
    content:
      't is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters',
    description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries",
    id: '3',
    publishedAt: 1595782632284,
    title: 'Hello Ivan 3',
  },
  {
    source: {
      id: '',
      name: '',
    },
    url: '',
    urlToImage: '',
    author: 'Aleh Serhiyenia',
    content:
      't is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters',
    description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries",
    id: '2',
    publishedAt: 729475225356,
    title: 'Hello Ivan 2',
  },
  {
    source: {
      id: '',
      name: '',
    },
    url: '',
    urlToImage: '',
    author: 'Aleh Serhiyenia',
    content:
      't is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters',
    description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries",
    id: '1',
    publishedAt: 335998800000,
    title: 'Hello Ivan 1',
  },
];

describe('function "sortByDate"', () => {
  it('should sort dates in descending order', () => {
    const newNews = mockNews.sort(sortByDate);
    expect(newNews).toEqual(mockNewsSort);
  });
});
