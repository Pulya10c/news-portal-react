import { INews } from '../common/models/news';

export const sortByDate = (prevNews: INews, nextNews: INews): number =>
  new Date(nextNews.publishedAt).getTime() - new Date(prevNews.publishedAt).getTime();
