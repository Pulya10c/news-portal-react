import React, { useState, ChangeEvent, FormEvent } from 'react';
import { INews } from '../../common/models/news';
import { history } from '../../utils/historyRouter';
import './FormNews.css';

interface IProps {
  news?: INews;
  handlerClick: (news: INews) => void;
}

const EMPTY_NEWS: INews = {
  source: {
    id: '',
    name: '',
  },
  title: '',
  author: '',
  description: '',
  content: '',
  id: '',
  url: '',
  urlToImage: '',
  publishedAt: new Date().getTime(),
};

const FormNews = (props: IProps): JSX.Element => {
  const { news, handlerClick } = props;
  const [editNews, setEditNews] = useState({ ...(news ? news : EMPTY_NEWS) });
  const [formValid, setFormValid] = useState(
    news
      ? {
          form: Boolean(news.title && news.author && news.description && news.content),
          title: Boolean(news.title),
          author: Boolean(news.author),
          description: Boolean(news.description),
          content: Boolean(news.content),
        }
      : {
          form: false,
          title: false,
          author: false,
          description: false,
          content: false,
        },
  );
  const [formErrors, setFormErrors] = useState({ title: '', author: '', description: '', content: '' });
  const [submitted, setSubmitted] = useState(false);

  const handleUserInput = ({ target }: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const name = target.name;
    const value = target.value;
    setEditNews({ ...editNews, [name]: value });
    validateField(name, value);
  };

  const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    setSubmitted(true);

    if (formValid.form) {
      handlerClick(editNews);
      history.push('/admin');
    }
  };

  const validateField = (fieldName: string, value: string) => {
    const fieldValidationErrors = formErrors;
    let titleValid = formValid.title;
    let authorValid = formValid.author;
    let descriptionValid = formValid.description;
    let contentValid = formValid.author;

    switch (fieldName) {
      case 'title':
        titleValid = value.length >= 3;
        fieldValidationErrors.title = titleValid ? '' : ' is invalid';
        break;
      case 'author':
        authorValid = value.length >= 5;
        fieldValidationErrors.author = authorValid ? '' : ' is invalid';
        break;
      case 'description':
        descriptionValid = value.length >= 15;
        fieldValidationErrors.description = descriptionValid ? '' : ' is invalid';
        break;
      case 'content':
        contentValid = value.length >= 6;
        fieldValidationErrors.content = contentValid ? '' : ' is invalid';
        break;
      default:
        break;
    }

    const validForm = titleValid && authorValid && descriptionValid && contentValid;

    setFormValid({
      title: titleValid,
      author: authorValid,
      description: descriptionValid,
      content: contentValid,
      form: validForm,
    });
    setFormErrors(fieldValidationErrors);
  };

  const errorClass = (error: boolean) => {
    return error ? 'has-error' : '';
  };

  return (
    <form className="form edit-news" onSubmit={handleSubmit}>
      <h2 className="form-name">{news ? 'Edit News' : 'Add news'}</h2>
      <div className={`form-group ${errorClass(!editNews.title && !formValid.title && submitted)}`}>
        <label htmlFor="title">Title</label>
        <input type="text" className="form-control" name="title" value={editNews.title} onChange={handleUserInput} />
        {submitted && !editNews.title && !formValid.title && <div className="help-block">title is required</div>}
      </div>
      <div className={`form-group ${errorClass(!formErrors.author && !formValid.author && submitted)}`}>
        <label htmlFor="author">Author</label>
        <input type="text" className="form-control" name="author" value={editNews.author} onChange={handleUserInput} />
        {submitted && !editNews.author && !formValid.author && <div className="help-block">author is required</div>}
      </div>
      <div className={`form-group ${errorClass(!formErrors.description && !formValid.description && submitted)}`}>
        <label htmlFor="description">Description</label>
        <textarea className="form-control" name="description" value={editNews.description} onChange={handleUserInput} />
        {submitted && !editNews.description && !formValid.description && (
          <div className="help-block">description is required</div>
        )}
      </div>
      <div className={`form-group ${errorClass(!formErrors.content && !formValid.content && submitted)}`}>
        <label htmlFor="content">Content</label>
        <textarea className="form-control" name="content" value={editNews.content} onChange={handleUserInput} />
        {submitted && !editNews.title && !formValid.title && <div className="help-block">content is required</div>}
      </div>
      <button type="submit" className="btn btn-primary">
        Save
      </button>
    </form>
  );
};

export default FormNews;
