import React from 'react';
import SearchPanel from '../search-panel';
import { TopNewsPanel } from '../top-news';
import { TITLE_TOP_NEWS, TITLE_CUSTOM_NEWS } from '../../common/constants';
import { ListLastNews } from '../list-last-news';
import { INews } from '../../common/models/news';

interface IProps {
  handlerEverythingNews: (query: string) => void;
  setBlockNews: (blockName: string) => void;
  lastNewsList: INews[];
}

const HomePage = (props: IProps): JSX.Element => {
  const { handlerEverythingNews, setBlockNews, lastNewsList } = props;

  const handlerBlockNews = (blockName: string) => () => setBlockNews(blockName);
  return (
    <div>
      <SearchPanel handlerEverythingNews={handlerEverythingNews} />
      <TopNewsPanel handlerBlockNews={handlerBlockNews('top')} title={TITLE_TOP_NEWS} />
      <TopNewsPanel handlerBlockNews={handlerBlockNews('custom')} title={TITLE_CUSTOM_NEWS} />
      <ListLastNews lastNewsList={lastNewsList} />
    </div>
  );
};

export default HomePage;
