import React from 'react';
import SearchPanel from './SearchPanel';
import renderer, { ReactTestRenderer } from 'react-test-renderer';
import { IProps } from './SearchPanel';

describe('Component InputText', () => {
  let defaultProps: IProps;
  let wrapper: ReactTestRenderer;

  beforeEach(() => {
    defaultProps = {
      handlerEverythingNews: jest.fn(),
    };
    wrapper = renderer.create(<SearchPanel {...defaultProps} />);
  });

  it('should render correctly', () => {
    expect(wrapper.toJSON()).toMatchSnapshot();
  });
});
