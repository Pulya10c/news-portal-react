import React, { useState, ChangeEvent, FormEvent } from 'react';
import './SearchPanel.css';

export interface IProps {
  handlerEverythingNews: (query: string) => void;
}

const SearchPanel = (props: IProps): JSX.Element => {
  const [query, setQuery] = useState('');
  const { handlerEverythingNews } = props;

  const changeQuery = ({ target }: ChangeEvent<HTMLInputElement>) => {
    setQuery(target.value);
  };

  const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    if (query) {
      handlerEverythingNews(query);
    }
  };

  return (
    <div>
      <form className="search-panel-form" onSubmit={handleSubmit}>
        <input
          className="form-control search-input search"
          placeholder="Type here to search"
          value={query}
          onChange={changeQuery}
        />
        <input type="submit" value="SEARCH" className="btn btn-secondary" />
      </form>
    </div>
  );
};

export default SearchPanel;
