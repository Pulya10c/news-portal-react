import React, { Dispatch, SetStateAction } from 'react';
import { INews } from '../../common/models/news';
import SearchPanel from '../search-panel';
import { NewsCards } from '../news-card';
import './NewsList.css';

type Props = {
  newsList: INews[];
  admin?: boolean;
  handlerEverythingNews: (query: string) => void;
  setOwnNews?: Dispatch<SetStateAction<INews>>;
  deleteNews?: (id: string) => void;
};

const NewsList = (props: Props): JSX.Element => {
  const { deleteNews, setOwnNews, newsList, handlerEverythingNews, admin } = props;
  const componentNewsList = newsList.length ? (
    newsList.map((news) =>
      !!setOwnNews && !!deleteNews ? (
        <NewsCards setOwnNews={setOwnNews} deleteNews={deleteNews} admin={admin} key={news.id} news={news} />
      ) : (
        <NewsCards admin={admin} key={news.id} news={news} />
      ),
    )
  ) : (
    <div className="alert alert-info no-match" role="alert">
      Sorry, no results were found for your search!
    </div>
  );

  return (
    <>
      {' '}
      {admin ? undefined : (
        <div>
          <SearchPanel handlerEverythingNews={handlerEverythingNews} />
          {newsList.length ? (
            <p className="search-count">
              Search <span className="count">{newsList.length}</span> News
            </p>
          ) : undefined}
        </div>
      )}
      <div className="news-list">{componentNewsList}</div>
    </>
  );
};

export default NewsList;
