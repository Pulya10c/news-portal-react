/* eslint-disable react/jsx-no-target-blank */
import React, { Dispatch, SetStateAction } from 'react';
import { Link } from 'react-router-dom';
import { history } from '../../utils/historyRouter';
import { INews } from '../../common/models/news';
import './NewsCards.css';
import { IEditNews } from '../../common/models/latestNews';

type Props = {
  news: INews | IEditNews;
  admin?: boolean;
  setOwnNews?: Dispatch<SetStateAction<INews>>;
  deleteNews?: (id: string) => void;
};

const NewsCards = (props: Props): JSX.Element => {
  const { deleteNews, setOwnNews, news, admin } = props;

  const handlerEditClick = () => {
    if (!!setOwnNews) {
      setOwnNews(news as INews);
      history.push(`admin/edit/${news.id}`);
    }
  };

  const handlerDeleteClick = () => {
    if (!!deleteNews) {
      deleteNews(news.id!);
      history.push(`admin/`);
    }
  };

  return (
    <div className="card news-list-cards">
      <div className="card-header" style={{ display: 'flex' }}>
        <span style={{ flex: '2' }}>{news.title}</span>
        {!admin ? (
          <span>
            {' '}
            <Link to={`news/${news.id}`} className="btn btn-outline-info btn-sm">
              <i className="fa fa-info-circle" aria-hidden="true" />
            </Link>
          </span>
        ) : (
          <div>
            <button onClick={handlerEditClick} className="btn btn-outline-success btn-sm">
              <i className="fa fa-pencil" aria-hidden="true" />
            </button>{' '}
            <button onClick={handlerDeleteClick} className="btn btn-outline-danger btn-sm">
              <i className="fa fa-trash-o" aria-hidden="true" />
            </button>
          </div>
        )}
      </div>
      <div className="card-body">
        <blockquote className="blockquote mb-0">
          <div className="about-news">
            {news?.urlToImage ? (
              <div className="card-img">
                <img className="card-img-left" src={news.urlToImage} alt="news" />
              </div>
            ) : undefined}
            <p style={{ fontSize: 'medium' }}>{news.description}</p>
          </div>
          <footer className="blockquote-footer">
            {admin || !news?.url ? undefined : (
              <span>
                Read more on{' '}
                <a href={news.url} target="_blank" rel="nofollow noopener">
                  <cite title="Source Title">{news.source?.name}</cite>
                </a>{' '}
                the site
              </span>
            )}
          </footer>
        </blockquote>
      </div>
    </div>
  );
};

export default NewsCards;
