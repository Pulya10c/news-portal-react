import React, { Dispatch, SetStateAction } from 'react';
import { NewsList } from '../news-list';
import { INews } from '../../common/models/news';
import { Link } from 'react-router-dom';
import './Admin.css';

interface IResponse {
  result: {
    status?: string;
    code?: string;
    message?: string;
    articles: INews[];
  };
}

interface IProps {
  setOwnNews: Dispatch<SetStateAction<INews>>;
  deleteNews?: (id: string) => void;
  newsList: INews[];
}

const Admin = (props: IProps): JSX.Element => {
  const { deleteNews, setOwnNews, newsList } = props;

  return (
    <div className="admin" style={{ margin: '40px 70px 0 70px' }}>
      <Link to={`admin/add/`} className="btn btn-success" style={{ marginBottom: '40px' }}>
        Add news
      </Link>
      <NewsList
        deleteNews={deleteNews!}
        setOwnNews={setOwnNews}
        handlerEverythingNews={() => console.log}
        admin={true}
        newsList={newsList}
      />
    </div>
  );
};

export default Admin;
