import React from 'react';
import { INews } from '../../common/models/news';
import './ListLastNews.css';
import { LastNewsCard } from '../last-news-card';

type Props = {
  lastNewsList: INews[];
};

const ListLastNews = (props: Props): JSX.Element => {
  const { lastNewsList } = props;
  const componentNewsList = lastNewsList.map((news) => <LastNewsCard key={news.id} news={news} />);

  return (
    <div>
      <span className="last-news-block">Last news</span>
      <div className="card-deck">{componentNewsList}</div>
    </div>
  );
};

export default ListLastNews;
