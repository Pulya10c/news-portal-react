import React, { useState } from 'react';
import { Route, Switch, Router, Redirect } from 'react-router-dom';
import { useAsync } from 'react-use';
import './App.css';
import { RequestService } from '../../services';
import { PATHS } from '../../common/constants';
import { NewsList } from '../news-list';
import { AboutNews } from '../about-news';
import { INews } from '../../common/models/news';
import { Spinner } from '../spinner';
import { HeaderApp } from '../header';
import { HomePage } from '../home-page';
import { Admin } from '../admin';
import { sortByDate } from '../../utils/utils';
import { EditNews } from '../form-News';
import { history } from '../../utils/historyRouter';
import { LoginPage } from '../loginPage';
import { NotificationComponent } from '../notification';

interface IResponse {
  result: {
    status?: string;
    code?: string;
    message?: string;
    id?: string;
    articles: INews[];
  };
}

const App = (): JSX.Element => {
  const [load, setLoad] = useState(false);
  const [isVisibleNotification, setIsVisibleNotification] = useState(false);
  const [isErrorResponse, setIsErrorResponse] = useState(false);
  const [newsList, setNewsList] = useState([] as INews[]);
  const [lastNews, setLatsNews] = useState([] as INews[]);
  const [ownNewsList, setOwnNewsList] = useState([] as INews[]);
  const [ownNews, setOwnNews] = useState({ title: '', author: '', description: '', content: '' } as INews);

  const changeNewsList = (blockName: string) => {
    setNewsList(blockName === 'custom' ? ownNewsList : newsList);
  };

  const showNotification = (isError: boolean) => {
    setIsErrorResponse(isError);
    setIsVisibleNotification(true);

    setTimeout(() => {
      setIsVisibleNotification(false);
    }, 3000);
  };

  useAsync(async () => {
    setLoad(true);
    try {
      const newsList: IResponse = await RequestService.getResponse({ url: '/news/top', method: 'GET' });

      const {
        result: { articles },
      } = newsList;

      setNewsList(newsList.result.status !== 'error' ? articles : []);

      const latestsNews = [...articles].sort(sortByDate);
      latestsNews.length = 4;
      setLatsNews(latestsNews);

      const ownNewsList: IResponse = await RequestService.getResponse({ url: '/news/own', method: 'GET' });
      setOwnNewsList(newsList.result.status !== 'error' ? ownNewsList.result.articles : []);
    } catch (err) {
      console.error(err);
    } finally {
      setLoad(false);
    }
  });

  const getEverythingNews = async (query: string) => {
    setLoad(true);
    try {
      const newsList: IResponse = await RequestService.getResponse({ url: '/news/everything', method: 'GET', query });

      const {
        result: { articles },
      } = newsList;

      setNewsList(newsList.result.status !== 'error' ? articles : []);
      history.push(PATHS.NEWS_LIST);
    } catch (err) {
      console.error(err);
    } finally {
      setLoad(false);
    }
  };

  const setUpdateNews = async (news: INews) => {
    setLoad(true);
    try {
      const newsList: IResponse = await RequestService.getResponse({ url: '/item', method: 'PUT', news });

      if (newsList.result.status !== 'error') {
        showNotification(false);
        console.log('Your request has been successfully saved!');
      } else {
        showNotification(true);
        console.log('Something went wrong. Please try again later!');
      }
    } catch (err) {
      console.error(err);
    } finally {
      setLoad(false);
    }
  };

  const createNews = async (news: INews) => {
    setLoad(true);
    try {
      const newsList: IResponse = await RequestService.getResponse({ url: '/new', method: 'POST', news });

      if (newsList.result.status !== 'error') {
        showNotification(false);
        console.log('Your request has been successfully saved!');
      } else {
        showNotification(true);
        console.log('Something went wrong. Please try again later!');
      }
    } catch (err) {
      console.error(err);
    } finally {
      setLoad(false);
    }
  };

  const deleteNews = async (id: string) => {
    setLoad(true);
    try {
      const response: IResponse = await RequestService.getResponse({ url: `/items/${id}`, method: 'DELETE' });

      if (response.result.status !== 'error') {
        showNotification(false);
        console.log('Your request has been successfully saved!');
      } else {
        showNotification(true);
        console.log('Something went wrong. Please try again later!');
      }
    } catch (err) {
      console.error(err);
    } finally {
      setLoad(false);
    }
  };

  return (
    <div className="container">
      <HeaderApp />
      <div style={{ maxWidth: '900px', margin: 'auto' }}>
        <Router history={history}>
          <Switch>
            <Route exact path={PATHS.HOME}>
              <HomePage
                lastNewsList={lastNews}
                setBlockNews={changeNewsList}
                handlerEverythingNews={getEverythingNews}
              />
            </Route>
            <Route path={PATHS.NEWS_LIST}>
              <NewsList handlerEverythingNews={getEverythingNews} newsList={newsList} />
            </Route>
            <Route exact path={PATHS.LOGIN}>
              <LoginPage />
            </Route>
            <Route path={PATHS.TOP_NEWS}>
              <NewsList handlerEverythingNews={getEverythingNews} newsList={newsList} />
            </Route>
            <Route exact path={PATHS.ADMIN}>
              <Admin newsList={ownNewsList} setOwnNews={setOwnNews} deleteNews={deleteNews} />
            </Route>
            <Route path={PATHS.EDIT}>
              <EditNews handlerClick={setUpdateNews} news={ownNews} />
            </Route>
            <Route path={PATHS.ADD}>
              <EditNews handlerClick={createNews} />
            </Route>
            <Route path={PATHS.CURRENT_NEWS}>
              <AboutNews newsList={newsList} />
            </Route>
            <Route exact path={PATHS.ANY}>
              <Redirect to={PATHS.HOME} />
            </Route>
          </Switch>
        </Router>
      </div>
      <Spinner load={load} />
      {isVisibleNotification && <NotificationComponent isError={isErrorResponse} />}
    </div>
  );
};

export default App;
