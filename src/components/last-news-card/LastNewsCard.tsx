/* eslint-disable react/jsx-no-target-blank */
import React from 'react';
import { history } from '../../utils/historyRouter';
import { INews } from '../../common/models/news';
import './LastNewsCard.css';
import { IEditNews } from '../../common/models/latestNews';

type Props = {
  news: INews | IEditNews;
};

const LastNewsCard = (props: Props): JSX.Element => {
  const { news } = props;
  const goToAboutNews = () => {
    history.push(`news/${news.id}`);
  };

  return (
    <div className="card last-news" onClick={goToAboutNews} style={{ width: '15rem' }}>
      <div className="card-body">
        <p className="card-text">{news.title}</p>
        {news?.url ? (
          <a href={news.url} className="card-link">
            Go to read...
          </a>
        ) : undefined}
      </div>
    </div>
  );
};

export default LastNewsCard;
