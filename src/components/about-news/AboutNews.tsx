import React from 'react';
import { INews } from '../../common/models/news';
import { useParams } from 'react-router-dom';
import './AboutNews.css';

type Props = {
  newsList: INews[];
};

const AboutNews = (props: Props): JSX.Element => {
  const { newsList } = props;
  const { id } = useParams();
  const news = newsList.find((news) => id === news.id);
  return (
    <div>
      {news ? (
        <div className="card" style={{ width: '100%', marginTop: '10px' }}>
          {news?.urlToImage ? <img className="card-img-top" src={news?.urlToImage} alt="news" /> : undefined}
          <div className="card-body about">
            {news?.title ? <h6 className="card-text article">Title</h6> : undefined}
            <p className="card-text about">{news.title}</p>
            {news?.description ? <h6 className="card-text article">Description</h6> : undefined}
            <p className="card-text about">{news.description}</p>
            {news?.content ? <h6 className="card-text article">Content</h6> : undefined}
            <p className="card-text about">{news.content}</p>
          </div>
        </div>
      ) : (
        <h3 className="no-content">No content</h3>
      )}{' '}
    </div>
  );
};

export default AboutNews;
