import React from 'react';
import Spinner from './Spinner';
import renderer from 'react-test-renderer';

describe('Component "Spinner"', () => {
  it('should render correctly with load=true', () => {
    const SpinnerComponent = renderer.create(<Spinner load={true} />).toJSON();
    expect(SpinnerComponent).toMatchSnapshot();
  });

  it('should render correctly with load=false', () => {
    const SpinnerComponent = renderer.create(<Spinner load={false} />).toJSON();
    expect(SpinnerComponent).toMatchSnapshot();
  });
});
