import React from 'react';
import ReactLoading from 'react-loading';
import './Spinner.css';

const Spinner = ({ load }: { load: boolean }): JSX.Element | null =>
  load ? <ReactLoading className="spinner" type="bubbles" color="blue" height={200} width={200} /> : null;

export default Spinner;
