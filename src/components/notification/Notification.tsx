import React from 'react';
import './Notification.css';

interface IProps {
  isError: boolean;
}

export const NotificationComponent = ({ isError }: IProps): JSX.Element => {
  return (
    <div
      className={
        'alert notification' + (isError ? ' alert-danger top-notification' : ' alert-success top-notification')
      }
      role="alert"
    >
      {isError ? 'Something went wrong. Please try again later!' : 'Your request has been successfully saved!'}
    </div>
  );
};
