import React from 'react';
import './HeaderApp.css';
import { history } from '../../utils/historyRouter';
import { PATHS } from '../../common/constants';

const handlerLogin = () => {
  history.push('/login');
};

const HeaderApp: () => JSX.Element = () => {
  const handlerRouter = () => {
    history.push(PATHS.HOME);
  };
  return (
    <div className="app-header d-flex">
      <h1 onClick={handlerRouter}>News portal</h1>
      <a onClick={handlerLogin} href="/login">
        <i className="fa fa-sign-in fa-2x" aria-hidden="true" />
      </a>
    </div>
  );
};

export default HeaderApp;
