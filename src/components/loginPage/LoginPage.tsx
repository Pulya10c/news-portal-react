import React, { useState, ChangeEvent, FormEvent } from 'react';
import './LoginPage.css';

function LoginPage(): JSX.Element {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [submitted, setSubmitted] = useState(false);

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    switch (name) {
      case 'username':
        setUsername(value);
        break;
      case 'password':
        setPassword(value);
        break;
      default:
        break;
    }
  };

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    setSubmitted(true);
    console.log(e);
  };

  return (
    <div className="col-md-6 col-md-offset-3" style={{ margin: '20px auto' }}>
      <form name="form" onSubmit={handleSubmit}>
        <h2 className="form-name">Login</h2>
        <div className={'form-group' + (submitted && !username ? ' has-error' : '')}>
          <label htmlFor="username">Username</label>
          <input type="text" className="form-control" name="username" value={username} onChange={handleChange} />
          {submitted && !username && <div className="help-block">Username is required</div>}
        </div>
        <div className={'form-group' + (submitted && !password ? ' has-error' : '')}>
          <label htmlFor="password">Password</label>
          <input type="password" className="form-control" name="password" value={password} onChange={handleChange} />
          {submitted && !password && <div className="help-block">Password is required</div>}
        </div>
        <div className="form-group">
          <button className="btn btn-primary">Login</button>
        </div>
      </form>
    </div>
  );
}

export default LoginPage;
