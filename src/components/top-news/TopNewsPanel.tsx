import React from 'react';
import './TopNewsPanel.css';
import { PATHS, TITLE_TOP_NEWS } from '../../common/constants';
import { history } from '../../utils/historyRouter';

export interface IProps {
  title: string;
  handlerBlockNews: () => void;
}

const TopNewsPanel = (props: IProps): JSX.Element => {
  const { title, handlerBlockNews } = props;
  const handlerRouter = () => {
    handlerBlockNews();
    history.push(PATHS.NEWS_LIST);
  };
  return (
    <div onClick={handlerRouter} className={`card news-headlines ${title === TITLE_TOP_NEWS ? 'top' : 'custom'}`}>
      <h4 className="card-text">{title}</h4>
    </div>
  );
};

export default TopNewsPanel;
