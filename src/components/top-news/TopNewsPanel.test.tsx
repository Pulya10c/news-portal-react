import React from 'react';
import TopNewsPanel, { IProps } from './TopNewsPanel';
import renderer from 'react-test-renderer';

describe('Component "TopNewsPanel"', () => {
  let defaultProps: IProps;

  beforeEach(() => {
    defaultProps = {
      title: 'custom',
      handlerBlockNews: jest.fn(),
    };
  });

  it('should render component correctly', () => {
    const TopNewsPanelComponent = renderer.create(<TopNewsPanel {...defaultProps} />).toJSON();
    expect(TopNewsPanelComponent).toMatchSnapshot();
  });
});
